import 'package:flutter/material.dart';

List<String> nomorSepatu = ["7", "8", "9", "10"];

List<Color> warna = [
  Color(0xFFF9362E),
  Color(0xFF003CFF),
  Color(0xFFFFB73A),
  Color(0xFF3AFFFF),
  Color(0xFF1AD12C),
  Color(0xFFD66400),
];

String deskripsi =
    "Dapatkan dukungan maksimal, kenyamanan, dan tampilan segar dengan sepatu energy cloud adidas yang dirancang untuk pria ini hadir dengan gaya klasik."
    "Tingkatkan kenyamanan lari Anda ke level selanjutnya dengan sepatu penyokong sintetis ini dengan sangkar kaki tengah FITFRAME agar pas dan terasa terkunci"
    "Penutupan renda-up Padded footed CLOUDFOAM midsole memberikan bantalan responsif sol karet ADIWEAR ™ Durable.";
